<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'name' => 'muhammad Damanhuri',
            'username' => 'dmhuri',
            'email' => 'damanhuri467@gmail.com',
            'password' => bcrypt(123456),
        ]);

        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-programming'
        ]);

        Category::Create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);

        Article::Create([
            'title' => 'Judul Pertama',
            'slug' => 'dudul-pertama',
            'category_id' => 1,
            'user_id' => 1,
            'exerpt' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum 2',
            'body' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum, eligendi facilis ullam et. Blanditiis, omnis nisi sequi totam ipsa necessitatibus obcaecati quia eum quod, at excepturi? Debitis esse ipsam explicabo velit, eveniet blanditiis perspiciatis enim quis soluta harum quae neque perferendis magnam sed cum. Atque at quod et ad harum, sunt expedita assumenda non, rem laboriosam nemo molestias optio consectetur ratione cumque deserunt doloribus quisquam animi architecto maiores excepturi eligendi? Fugit facilis consectetur, vitae possimus natus et voluptatibus officiis quisquam, laborum sit blanditiis! Magni id distinctio fugit ab sint.'
        ]);

        Article::Create([
            'title' => 'Judul Kedua',
            'slug' => 'dudul-kedua',
            'category_id' => 1,
            'user_id' => 1,
            'exerpt' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum 2',
            'body' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum, eligendi facilis ullam et. Blanditiis, omnis nisi sequi totam ipsa necessitatibus obcaecati quia eum quod, at excepturi? Debitis esse ipsam explicabo velit, eveniet blanditiis perspiciatis enim quis soluta harum quae neque perferendis magnam sed cum. Atque at quod et ad harum, sunt expedita assumenda non, rem laboriosam nemo molestias optio consectetur ratione cumque deserunt doloribus quisquam animi architecto maiores excepturi eligendi? Fugit facilis consectetur, vitae possimus natus et voluptatibus officiis quisquam, laborum sit blanditiis! Magni id distinctio fugit ab sint.'
        ]);

        Article::Create([
            'title' => 'Judul Ketiga',
            'slug' => 'dudul-ketiga',
            'category_id' => 2,
            'user_id' => 1,
            'exerpt' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum 2',
            'body' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum, eligendi facilis ullam et. Blanditiis, omnis nisi sequi totam ipsa necessitatibus obcaecati quia eum quod, at excepturi? Debitis esse ipsam explicabo velit, eveniet blanditiis perspiciatis enim quis soluta harum quae neque perferendis magnam sed cum. Atque at quod et ad harum, sunt expedita assumenda non, rem laboriosam nemo molestias optio consectetur ratione cumque deserunt doloribus quisquam animi architecto maiores excepturi eligendi? Fugit facilis consectetur, vitae possimus natus et voluptatibus officiis quisquam, laborum sit blanditiis! Magni id distinctio fugit ab sint.'
        ]);

    }
}
