@extends("layouts.main")

@section('isihalaman')
    <h1 class="mb-3">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-6">
            <form action="/articles" method="GET">
                @if (request('category'))
                    <input type="hidden" name="category" value="{{ request('category') }}">
                @endif
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search" name="search" value="{{ request('search') }}">
                    <button class="btn btn-danger" type="submit">Search</button>
                  </div>
            </form>
        </div>
    </div>
    @foreach ($posts as $post)
    <article class="mb-5">
        <h2><a href="/single_post/{{ $post->slug }}" class="text-decoration-none">{{ $post->title }}</a></h2> 
        <h5>By <a href="/author/{{ $post->user->name }}" class="text-decoration-none">{{ $post->user->name }}</a> in <a href="/articles?category={{ $post->category->slug }}" class="text-decoration-none">{{ $post->category->name }}</a>  </h5>
        <p>{{ $post->exerpt }}</p>
    </article>
    @endforeach

    <div class="d-flex justify-content-end">
        {{ $posts->links() }}
    </div>
    
@endsection

