@extends("layouts.main")

@section('isihalaman')
    <h1 class="mb-5">Post Category : {{ $category }} </h1>
    @foreach ($posts as $post)
    <article class="mb-5">
        <a href="/single_post/{{ $post->slug }}"><h2>{{ $post->title }}</h2></a> 
        <h5>By : {{ $post->author }}</h5>
        <p>{{ $post->exerpt }}</p>
    </article>
    @endforeach
@endsection

{{-- 
Article::create([
    'title' => 'Judul Kedua',
    'slug' => 'judul-kedua',
    'exerpt' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum 2',
    'body' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae quibusdam, minima aut a libero nostrum debitis excepturi obcaecati voluptatem earum, eligendi facilis ullam et. Blanditiis, omnis nisi sequi totam ipsa necessitatibus obcaecati quia eum quod, at excepturi? Debitis esse ipsam explicabo velit, eveniet blanditiis perspiciatis enim quis soluta harum quae neque perferendis magnam sed cum. Atque at quod et ad harum, sunt expedita assumenda non, rem laboriosam nemo molestias optio consectetur ratione cumque deserunt doloribus quisquam animi architecto maiores excepturi eligendi? Fugit facilis consectetur, vitae possimus natus et voluptatibus officiis quisquam, laborum sit blanditiis! Magni id distinctio fugit ab sint.'
]) --}}