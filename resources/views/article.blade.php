@extends('layouts.main')
@section('isihalaman')
    <h2>{{ $post->title }}</h2>
    <p>By. {{ $post->user->name }} in <a href="/articles?category={{ $post->category->slug }}"> {{ $post->category->name }} </a></p>
    <h5>By : {{ $post->author }}</h5>
    {!! $post->body !!}
    <a href="/articles">Back to Posts</a>
@endsection