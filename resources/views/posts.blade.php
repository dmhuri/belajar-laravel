@extends('layouts.main')
@section('isihalaman')
    <article class="mb-5">
        <a href="/post/{{ $post['slug'] }}"><h2>{{ $post['title'] }}</h2></a> 
        <h5>By : {{ $post['author'] }}</h5>
        <p>{{ $post['body'] }}</p>
    </article>
    <a href="/blog">Back to Posts</a>
@endsection