

@extends('layouts.main')

@section('isihalaman')
<div class="row justify-content-center">
    <div class="col-md-6">
        <main class="form-registration">
            <h1 class="h3 mb-3 fw-normal">Registration Form</h1>
            <form action="/register" method="POST">
                @csrf
                <div class="form-floating">
                    <input type="text" class="form-control rounded-top @error('name') is-invalid @enderror" id="Name" placeholder="Name" name="name" value="{{ old('name') }}" required>
                    <label for="floatingInput">Name</label>
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-floating">
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="Username" placeholder="Username" name="username" value="{{ old('username') }}" required>
                    <label for="floatingInput">Username</label>
                    @error('username')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-floating">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput" placeholder="name@example.com" name="email" value="{{ old('email') }}" required>
                    <label for="floatingInput">Email address</label>
                     @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control rounded-bottom @error('password') is-invalid @enderror" id="Password" placeholder="Password"  name="password" required>
                    <label for="floatingPassword">Password</label>
                     @error('password')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">Register</button>
            </form>
            <small class="d-block mt-3 text-center">Alredady Registered? <a href="/login">Login</a></small>
        </main>
    </div>
</div>
@endsection