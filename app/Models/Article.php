<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    //properti agar bisa di isi pake function mess assingment seperti create dan update, jika tidak ada di fillable, akan di isi default value
    // protected $fillable = ['title','exerpt','body'];

    //properti agar tidak boleh di isi, properti lain boleh
    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, array $filters){ //nama scope function harus diawali dengan kata scope kemudian namafunction, misal scopeNamafunction
        // if(isset($filters['search']) ? $filters['search'] : false){
        //     return $query->where('title','like', '%'.$filters['search'].'%')
        //             ->orWhere('body', 'like', '%'.$filters['search'].'%');
        // }

        //null coalescing operator, $variable ?? "value if not set or null"
        $query->when($filters['search'] ?? false, function($query, $search) {
            return $query->where(function($query) use ($search) {
                 $query->where('title', 'like', '%' . $search . '%')
                             ->orWhere('body', 'like', '%' . $search . '%');
             });
         });

        //join query
        $query->when($filters['category'] ?? false, function($query, $category){
            return $query->whereHas('category', function($query) use($category){
                $query->where('slug', $category);
            });
        }); 
    
    }
}
