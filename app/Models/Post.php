<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;

class Post 
{
    private static $blog_posts = [
        [
            "title" => "Judul Post Pertama",
            "slug" => "judul-post-pertama",
            "author" => "Dede Ganteng",
            "body" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis esse, fuga dolore a magnam voluptatum, earum dignissimos itaque, repudiandae voluptates aut nobis. Consequatur ut commodi praesentium illum! Totam, quasi eveniet. Facilis id natus repudiandae quasi quis nesciunt obcaecati ratione pariatur iure dolor voluptatibus quam et veritatis at ex ad saepe perspiciatis dicta, nobis ducimus beatae enim a aut deserunt. Sed quasi fuga tenetur ad exercitationem accusamus odit repellendus quis iste necessitatibus fugiat error dolore odio vitae, facere vero unde illo veritatis doloribus qui iusto sequi! Iure a fuga reprehenderit asperiores!",
        ],
        [
            "title" => "Judul Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "Doddy Mulyadi",
            "body" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis esse, fuga dolore a magnam voluptatum, earum dignissimos itaque, repudiandae voluptates aut nobis. Consequatur ut commodi praesentium illum! Totam, quasi eveniet. Facilis id natus repudiandae quasi quis nesciunt obcaecati ratione pariatur iure dolor voluptatibus quam et veritatis at ex ad saepe perspiciatis dicta, nobis ducimus beatae enim a aut deserunt. Sed quasi fuga tenetur ad exercitationem accusamus odit repellendus quis iste necessitatibus fugiat error dolore odio vitae, facere vero unde illo veritatis doloribus qui iusto sequi! Iure a fuga reprehenderit asperiores!",
        ],
    ];

    public static function all(){
        return collect(self::$blog_posts); //self untuk mengakses properti static
    }

    public static function find($slug){
        $posts = self::$blog_posts; //static untuk mengakses method static
        $post = [];
        foreach($posts as $p){
            if($p["slug"] === $slug){
                $post = $p;
            }
        }
        return $post;
    }

    //collection, mengubah array agar bisa menjalankan banyak function2 yang ada di collection

    public static function find_collection($slug){
        $posts = static::all(); //static untuk mengakses method static
        return $posts->firstWhere("slug", $slug);
    }
}
