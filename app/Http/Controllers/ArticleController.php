<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    // public function index(){
    //     return view('articles',[
    //         "title" => "All Post",
    //         // "posts" => Article::all(),
    //         "posts" => Article::with(['user','category'])->latest()->get() //eiger loading, menghindari N+1 problem
    //     ]);
    // }

    public function index(){
        // $articles = Article::with(['user','category'])->latest(); //eiger loading, menghindari N+1 problem

        // if(request('search')){
        //     $articles->where('title','like', '%'.request('search').'%')
        //              ->orWhere('body', 'like', '%'.request('search').'%');
        // }

        //pindah ke scope model
        $title = '';
        if(request('category')){
            $category = Category::firstWhere('slug',request('category'));
            $title = ' in '.$category->name;
        }
        return view('articles',[
            "title" => "All Post ".$title,
            // "posts" => Article::all(),
            // "posts" => $articles->get() 
            // "posts" => Article::with(['user','category'])->filter(request(['search','category']))->latest()->get() //function filter diambil dari scopeFilter
            "posts" => Article::with(['user','category'])->filter(request(['search','category']))->latest()->paginate(3)->withQueryString() //function filter diambil dari scopeFilter
        ]);
    }

    public function show($id){
        return view('article',[
            "title" => "Blog",
            "post" => Article::find($id),
        ]);
    }

    public function tampil(Article $article){ //post model binding, model dijadikan parameter, memugkinkan paramater yang dicari tidak berdasarkan id
        return view('article',[
            "title" => "All Blog",
            "post" => $article,
        ]);
    }
    
}
