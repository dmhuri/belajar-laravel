<?php

use App\Models\Post;
use App\Models\Category;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home',[
        "title" => "Home"
    ]);
});

Route::get('/about',function(){
    return view('about',[
        "title" => "About",
        "name" => "Muhamad Damanhuri",
        "email" => "nyariapa.id@gmail.com",
        "image" => "logo-wifiboss-router.png"
    ]);
});

Route::get("/blog",function(){
    return view('post',[
        "title" => "Blog",
        "posts" => Post::all(),
    ]);
});

Route::get('post/{slug}', function($slug){
    return view("posts",[
        "title" => "Single Post",
        "post" => Post::find_collection($slug),
    ]);
});


//contoh pake controller
Route::get("/blogs",[PostController::class, 'index']);
Route::get("/single_page/{slug}",[PostController::class, 'show']);
Route::get("/articles",[ArticleController::class, 'index']);
Route::get("/article/{id}",[ArticleController::class, 'show']);

//route model binding, untuk dapat insatance dari model
Route::get("/single_post/{article:slug}",[ArticleController::class, 'tampil']);

Route::get('/category/{category:slug}', function(Category $category){
    return view('articles',[
        'title' => "Post Category ".$category->name,
        // 'posts' => $category->article, //lazy load
        'posts' => $category->article->load("category","user"), //eiger lazy load
        'category' => $category->name,
    ]);
});

Route::get('/author/{user}', function(User $author){
    return view('articles',[
        'title' => "Post by ".$author->name,
        'posts' => $author->article,
        'category' => $author->name,
    ]);
});

//login register
Route::get("/login", [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post("/login", [LoginController::class, 'authenticate']);
Route::get("/register", [RegisterController::class, 'index'])->middleware('guest');
Route::post("/register", [RegisterController::class, 'store']);
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth');
Route::post("/logout", [LoginController::class, 'logout']);